//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//  Modified by dm.kulinchenko on 15/11/16.

#import "SPIPlayerSpaceship.h"

@implementation SPIPlayerSpaceship

- (instancetype)initWithName:(NSString *)name Position:(NSUInteger)position {
    self = [super init];
    if (self) {
        _name = name;
        _positionStarSystem = position;
    }
    return self;
}

- (void)loadStarSystem2:(NSArray *)starSystem2 {
    if(starSystem2 != _starSystem2) {
        _starSystem2 = starSystem2;
    }
    self.starSystem = [starSystem2 objectAtIndex:_positionStarSystem];
}

- (void)loadStarSystem:(SPIStarSystem *)starSystem {
    _starSystem = starSystem;
    _currentSpaceObject = _starSystem.spaceObjects.firstObject;
}

- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];

    printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
    printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(command, 255, stdin);
    int commandNumber = atoi(command);
    self.nextCommand = commandNumber;
    
    return self.nextCommand == SPIPlayerSpaceshipCommandExit ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
}

- (void)nextTurn {
    
    switch (self.nextCommand) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex > 0) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1];
            }
            else if ( (currentSpaceObjectIndex == 0 ) && (self.positionStarSystem <= self.starSystem2.count - 1) && (self.positionStarSystem != 0) ) {
                self.starSystem = [self.starSystem2 objectAtIndex:_positionStarSystem-1];
                self.positionStarSystem = _positionStarSystem - 1;
                self.currentSpaceObject = self.starSystem.spaceObjects.lastObject;
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            printf("%s\n", [[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                self.currentSpaceObject = [self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1];
            }
            else if ( (currentSpaceObjectIndex == (self.starSystem.spaceObjects.count - 1) ) && (self.positionStarSystem < self.starSystem2.count - 1) ) {
                self.starSystem = [self.starSystem2 objectAtIndex:_positionStarSystem+1];
                self.positionStarSystem = _positionStarSystem + 1;
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandDestroyObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if(!self.currentSpaceObject.notDestroy) {
                _namePrevObject = self.currentSpaceObject.name;
                
                switch (self.currentSpaceObject.type) {
                        
                    case SPISpaceObjectTypePlanet: {
                        SPIAsteroidField *newAsteroidField = [[SPIAsteroidField alloc] initWithName:_namePrevObject];
                        
                        [_starSystem.spaceObjects replaceObjectAtIndex: [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject] withObject:newAsteroidField];
                        _currentSpaceObject = newAsteroidField;
                        break;
                    }
                        
                    case SPISpaceObjectTypeStar: {
                        SPIPlanet *newPlanet = [[SPIPlanet alloc] initWithName:_namePrevObject];
                        
                        [_starSystem.spaceObjects replaceObjectAtIndex: [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject] withObject:newPlanet];
                        _currentSpaceObject = newPlanet;
                        break;
                    }
                        
                    case SPISpaceObjectTypeAsteroidField: {
                        if (self.starSystem.spaceObjects.count == 1) {
                            NSLog(@"Current StarSystem is empty...");
                        }
                        else {
                            NSLog(@"'%@' is destroy!", _currentSpaceObject.name);
                            [_starSystem.spaceObjects removeObject:self.currentSpaceObject];
                            _currentSpaceObject = [_starSystem.spaceObjects objectAtIndex:0];
                        }
                        break;
                    }
                        
                    default:
                        break;
                }
                
                break;
            }
            else {
                NSLog(@"'%@' do not destroy!", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex] title]);
                break;
            }
        }
            
        default:
            break;
    }
    self.nextCommand = 0;
}

- (NSString *)availableCommands {
    if (self.starSystem) {
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        for (NSInteger command = SPIPlayerSpaceshipCommandFlyToPreviousObject; command <= SPIPlayerSpaceshipCommandFlyToNextObject; command++) {
            NSString *description = [self descriptionForCommand:command];
            if (description) {
                [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)command, description]];
            }
        }
        
        NSString *description = [self descriptionForCommand:SPIPlayerSpaceshipCommandExit];
        if (description) {
            [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld. %@", (long)SPIPlayerSpaceshipCommandExit, description]];
        }
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}

- (NSString *)descriptionForCommand:(SPIPlayerSpaceshipCommand)command {
    NSString *commandDescription = nil;
    switch (command) {
        case SPIPlayerSpaceshipCommandFlyToPreviousObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex == 0 && self.positionStarSystem > 0) {
                commandDescription = [NSString stringWithFormat:@"Move to prevGalaktics '%@'", [[self.starSystem2 objectAtIndex:_positionStarSystem-1] name]];
            }
            else if (currentSpaceObjectIndex > 0) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex - 1] title]];
            }
            else if (self.positionStarSystem > 0) {
                currentSpaceObjectIndex = [self.starSystem.spaceObjects count] - 1;
                commandDescription = [NSString stringWithFormat:@"Move to prevGalaktics '%@' to %@", [self.starSystem name], [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex] title]];
            }
            break;
        }
            
        case SPIPlayerSpaceshipCommandExploreCurrentObject: {
            commandDescription = [NSString stringWithFormat:@"Explore %@", [self.currentSpaceObject title]];
            break;
        }
            
        case SPIPlayerSpaceshipCommandFlyToNextObject: {
            NSInteger currentSpaceObjectIndex = [self.starSystem.spaceObjects indexOfObject:self.currentSpaceObject];
            if (currentSpaceObjectIndex < (self.starSystem.spaceObjects.count - 1)) {
                commandDescription = [NSString stringWithFormat:@"Fly to %@", [[self.starSystem.spaceObjects objectAtIndex:currentSpaceObjectIndex + 1] title]];
            }
            else if (self.positionStarSystem < ([self.starSystem2 count] - 1) ) {
                commandDescription = [NSString stringWithFormat:@"Move to nextGalaktics '%@'", [[self.starSystem2 objectAtIndex:_positionStarSystem+1] name]];
            }

            break;
        }
            
        case SPIPlayerSpaceshipCommandDestroyObject: {
            commandDescription = [NSString stringWithFormat:@"Destroy %@", [self.currentSpaceObject title]];
            break;
        }
            
        case SPIPlayerSpaceshipCommandExit: {
            commandDescription = [NSString stringWithFormat:@"Exit"];
            break;
        }
            
        default:
            break;
    }
    return commandDescription;
}

@end
