//
//  SPISpaceStar.h
//  Lection2
//
//  Created by dm.kulinchenko on 15.11.16.
//  Copyright © 2016 dm.kulinchenko. All rights reserved.
//

#import "SPISpaceObject.h"

typedef NS_ENUM(NSInteger, SPIStarType) {
    SPIStarType1,
    SPIStarType2,
    SPIStarType3,
};

@interface SPIStar : SPISpaceObject

@property (nonatomic, assign, readonly) SPIStarType type_star;
@property (nonatomic, assign) NSNumber *weight;
@property (nonatomic, assign) NSString *name;
@property (nonatomic) BOOL notDestroy;

- (instancetype)initWithStar:(SPIStarType)type_star name:(NSString *) name weight:(NSNumber *) weight;

@end
