//
//  PlayerSpaceship.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//  Modified by dm.kulinchenko on 15/11/16.

#import "SPISpaceObject.h"
#import "SPIStarSystem.h"
#import "SPIGameObject.h"
#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIStar.h"

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipCommand) {
    SPIPlayerSpaceshipCommandFlyToPreviousObject = 1,
    SPIPlayerSpaceshipCommandExploreCurrentObject,
    SPIPlayerSpaceshipCommandDestroyObject,
    SPIPlayerSpaceshipCommandFlyToNextObject,
    SPIPlayerSpaceshipCommandExit = 42,
};

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipResponse) {
    SPIPlayerSpaceshipResponseOK = 0,
    SPIPlayerSpaceshipResponseExit,
};

@interface SPIPlayerSpaceship : NSObject <SPIGameObject>

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic) NSString *namePrevObject;

@property (nonatomic, assign) SPIPlayerSpaceshipCommand nextCommand;

@property (nonatomic, weak, setter=loadStarSystem:) SPIStarSystem *starSystem;
@property (nonatomic, weak, setter=loadStarSystem2:) NSArray *starSystem2;
@property (nonatomic, assign) NSUInteger positionStarSystem;
@property (nonatomic, weak) SPISpaceObject *currentSpaceObject;

- (instancetype)initWithName:(NSString *)name Position:(NSUInteger)position;

- (NSString *)availableCommands;

- (SPIPlayerSpaceshipResponse)waitForCommand;

@end
