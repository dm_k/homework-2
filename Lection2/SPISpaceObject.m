//
//  SpaceObject.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 05/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//  Modified by dm.kulinchenko on 15/11/16.

#import "SPISpaceObject.h"

@implementation SPISpaceObject

+ (NSString *)typeStringForType:(SPISpaceObjectType)type {
    switch (type) {
        case SPISpaceObjectTypePlanet:
            return @"Planet";
            
        case SPISpaceObjectTypeAsteroidField:
            return @"Asteroid field";
            
        case SPISpaceObjectTypeStar:
            return @"Star";
            
        case SPISpaceObjectTypePlayerSpaceship:
            return @"Spaceship";
        
        default:
            return @"Unknown";
    }
}

- (instancetype)initWithType:(SPISpaceObjectType)type name:(NSString *)name {
    self = [super init];
    if (self) {        
        _type = type;
        _name = name;
    }
    return self;
}
- (NSString *)title {
    return [NSString stringWithFormat:@"%@ %@", [SPISpaceObject typeStringForType:self.type], self.name];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ named %@. Turn %ld", [SPISpaceObject typeStringForType:self.type], self.name, (long)self.turn];
}

- (void)nextTurn {
    self.turn++;
}

@end
