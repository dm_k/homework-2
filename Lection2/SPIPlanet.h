//
//  Planet.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 10/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@interface SPIPlanet : SPISpaceObject

@property (nonatomic, assign, getter=hasAtmosphere) BOOL atmosphere;
@property (nonatomic, assign, getter=hasPeoples, readonly) BOOL peoples;
@property (nonatomic, assign) NSInteger peoplesCount;
@property (nonatomic) BOOL notDestroy;

- (instancetype)initWithName:(NSString *)name;

@end
