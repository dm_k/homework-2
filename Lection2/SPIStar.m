//
//  SPISpaceStar.m
//  Lection2
//
//  Created by dm.kulinchenko on 15.11.16.
//  Copyright © 2016 dm.kulinchenko. All rights reserved.
//

#import "SPIStar.h"

@implementation SPIStar

+ (NSString *)typeStringForTypeStar:(SPIStarType)type_star {
    switch (type_star) {
        case SPIStarType1:
            return @"A-type";
            
        case SPIStarType2:
            return @"B-type";
            
        case SPIStarType3:
            return @"C-type";
            
        default:
            return @"Unknown";
    }
}

- (instancetype)initWithStar:(SPIStarType)type_star name:(NSString *)name weight:(NSNumber *)weight{
    self = [super initWithType:SPISpaceObjectTypeStar name: name];
    if (self) {
        _type_star = type_star;
        _weight = weight;
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"\nStarType: %@\nWeight: %@", [SPIStar typeStringForTypeStar:self.type_star], self.weight];
}

- (void)nextTurn {
    [super nextTurn];
}

@end
