//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//  Modified by dm.kulinchenko on 15/11/16.
#import <stdio.h>

#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"

#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"
#import "SPIStar.h"

int main(int argc, const char * argv[]) {
    SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius" age:@(230000000)];
    SPIStarSystem *bettaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius_Betta" age:@(125555454)];
 
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan"];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 325000000;
    vulkanPlanet.notDestroy = YES;

    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota"];
    hotaAsteroidField.density = 6000000;
    hotaAsteroidField.notDestroy = NO;

    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey"];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    gallifreiPlanet.notDestroy = NO;
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo"];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    nabooPlanet.notDestroy = NO;
    
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto"];
    plutoPlanet.atmosphere = NO;
    plutoPlanet.notDestroy = NO;
    
    SPIStar *sunStar = [[SPIStar alloc] initWithStar:SPIStarType1 name:@"Sun" weight:@(45000)];
    sunStar.notDestroy = YES;
    
    SPIStar *star1 = [[SPIStar alloc] initWithStar:SPIStarType2 name:@"Star1" weight:@(8000)];
    star1.notDestroy = NO;
    
    SPIStar *star2 = [[SPIStar alloc] initWithStar:SPIStarType3 name:@"Star" weight:@(78000)];
    star2.notDestroy = NO;
    
    alphaStarSystem.spaceObjects = [@[vulkanPlanet, hotaAsteroidField, gallifreiPlanet, nabooPlanet, sunStar, plutoPlanet] mutableCopy];
    bettaStarSystem.spaceObjects = [@[star1, gallifreiPlanet, star2] mutableCopy];
    
    NSArray *starsSystem = [[NSArray alloc] initWithObjects:alphaStarSystem, bettaStarSystem, nil];

    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Falcon" Position:0];
    [spaceship loadStarSystem2:starsSystem];
 
    BOOL play = YES;
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:bettaStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];

    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}
